### 前言

肝代码不易，若是在网上发表请标明出处，另外请路过的朋友们随手点个Star。本项目仅用于学习。

###  项目介绍

基于桌面端的网易云app搭建的web音乐播放器

此项目主要是使用vue3+ts从项目规范角度搭建的符合实际开发规范项目，可供大家参考

大家对项目搭建及开发规范感兴趣的请移步[我的文章](https://juejin.cn/post/7215401973843394619)

[全功能演示视频地址](【vue网易云音乐web端在线音乐播放器】 https://www.bilibili.com/video/BV1ms4y1m7bL/?share_source=copy_web&vd_source=83dcd85b8351751639167820b70d8671)

[项目地址](https://gitee.com/liukun-uzi/netease-cloud-web-music-app-masterhttps://gitee.com/liukun-uzi/netease-cloud-music-uniapp)

[项目体验地址](http://liukun-uzi.gitee.io/netease-cloud-web-music-app/#/individuation)

目前项目只有一个分支 master

###  后端API

本项目后端所有数据来自开源项目NeteaseCloudMusicApi，下面附有后端开源项目地址、文档

[后端API仓库地址](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2FBinaryify%2FNeteaseCloudMusicApi)

[API文档](https://gitee.com/link?target=https%3A%2F%2Fneteasecloudmusicapi.vercel.app%2F%23%2F)

###  运行项目

1. 运行后端仓库

   1.1. git clone 后端仓库地址

   1.2. npm install

   1.3. node app

2. 运行本仓库代码

   1.1 git clone https://gitee.com/liukun-uzi/netease-cloud-web-music-app-master.git

   1.2 cd netease-cloud-web-music-app-master
   
   1.3 npm install
   
   1.4 npm run serve

###  问题交流群

欢迎加群一起讨论  q群：811937159
###  项目效果预览

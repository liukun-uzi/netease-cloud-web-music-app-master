export type AudioPlayerOption = {
  src: string
  autoPlay?: boolean
  progressBarColor?: string //progress bar color
  indicatorColor?: string //indicator color
  volume?: number
}

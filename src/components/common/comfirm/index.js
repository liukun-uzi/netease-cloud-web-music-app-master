import { createApp } from 'vue'
import message from './message.vue'
let $vm
let messagePlugin = {}

messagePlugin.install = function (app) {
  let defaultOptions = {
    confirmButtonText: '确定',
    cancelButtonText: '取消',
    type: 'warning',
    vNode: false
  }

  const confirm = function (title, msg, options) {
    const newOption = Object.assign(
      defaultOptions,
      {
        title: title,
        msg: msg
      },
      options
    )
    const className = 'pop-up'
    let container
    if (document.querySelector(`.${className}`)) {
      container = document.querySelector(`.${className}`)
    } else {
      container = document.createElement('div')
      container.setAttribute('class', className)
    }
    let plugin = createApp(message, newOption, null)
    $vm = plugin.mount(container)
    document.body.appendChild($vm.$el)
    // let $vm = createVNode(message, newOption, null) // 第二种挂在全局的方式
    // render($vm, container)
    // document.body.append(container)
    // return $vm.component?.proxy.confirm()
    return $vm.confirm()
  }

  app.config.globalProperties.$confirm = confirm
}

export default messagePlugin

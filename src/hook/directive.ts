import { useIntersectionObserver } from '@vueuse/core'

const defineDirective = (app) => {
  app.directive('lazy', {
    mounted(el: HTMLImageElement, { value }) {
      // const {stop} = useIntersectionObserver(target, fn, options)
      const { stop } = useIntersectionObserver(
        el,
        ([{ isIntersecting }]) => {
          if (isIntersecting) {
            el.src = value
            stop()
          }
        },
        {
          rootMargin: '400px',
          threshold: [0]
        }
      )
    }
  })
}

export default {
  install(app) {
    defineDirective(app)
  }
}

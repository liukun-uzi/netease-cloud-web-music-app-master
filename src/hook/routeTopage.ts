import { router } from '@/router'

const toPage = (pageName: string, params: any) => {
  router.push({
    name: pageName,
    params
  })
}

export { toPage }

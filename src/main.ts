import { createApp } from 'vue'
import { router } from './router'
import { setupStore } from './store'

import './assets/scss/base.scss'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'
import './assets/scss/icon.scss'
import directive from './hook/directive'
import App from './App.vue'

const app = createApp(App)
app.use(router)
setupStore(app)
directive.install(app)
app.mount('#app')

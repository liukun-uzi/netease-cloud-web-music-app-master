import { type Router, createRouter, createWebHashHistory } from 'vue-router'
const routes: any = [
  {
    path: '/',
    redirect: '/individuation'
  },
  {
    path: '/individuation',
    name: 'individuation',
    component: async () => await import('@/views/individuation/index.vue')
  },
  {
    path: '/songList',
    name: 'songList',
    component: async () => await import('@/views/songList/index.vue')
  },
  {
    path: '/rankList',
    name: 'rankList',
    component: async () => await import('@/views/rankList/index.vue')
  },
  {
    path: '/singer',
    name: 'singer',
    component: async () => await import('@/views/singer/index.vue')
  },
  {
    path: '/playlist/:id',
    name: 'playlist',
    component: async () => await import('@/views/playlist/index.vue')
  }
]

export const router: Router = createRouter({
  history: createWebHashHistory(),
  routes
})

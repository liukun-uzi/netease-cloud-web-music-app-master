import { AxiosInstance, AxiosRequestConfig, Method } from 'axios'

export interface MusicHttpResponse extends AxiosResponse {
  data: AxiosResponse<any, any> | Promise<AxiosResponse<any, any>>
  config: MusicRequestConfig
}

export interface MusicRequestConfig extends AxiosRequestConfig {
  beforeRequestCallback?: (request: MusicRequestConfig) => void
  beforeResponseCallback?: (response: MusicHttpResponse) => void
}

export type RequestMethods = Extract<
  Method,
  'get' | 'post' | 'put' | 'delete' | 'patch' | 'option' | 'head'
>

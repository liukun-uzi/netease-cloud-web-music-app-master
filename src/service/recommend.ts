import http from './http/request'

type dataType = {
  data?: any
  code: number
  banners?: any[]
  result?: any
  recommend: any[]
  playlists?: any
  tags?: any[]
  categories?: string[]
  sub?: any[]
  all?: any
  list?: any[]
}
/**
 *  轮播
 * @param type
 *  0: pc
    1: android
    2: iphone
    3: ipad
 * @returns
 */
export const _banner = (params?: object) => {
  return http.request<dataType>('get', '/banner', { params })
}

/**
 *  推荐歌单（登录）
 * @param params limit
 * @returns
 */
export const _recommendResource = (params?: object) => {
  return http.request<dataType>('get', '/recommend/resource', { params })
}

/**
 *  推荐歌曲（登录）
 * @param params
 * limit
 * @returns
 */
export const _recommendSongs = (params?: object) => {
  return http.request<dataType>('get', '/recommend/songs', { params })
}

/**
 *  歌单分类
 * @param params
 * @returns
 */
export const _catlist = () => {
  return http.request<dataType>('get', '/playlist/catlist')
}

/**
 *  热门歌单分类
 * @param params
 * @returns
 */
export const _hotCatlist = () => {
  return http.request<dataType>('get', '/playlist/hot')
}

/**
 *  网友精选歌单
 * @param params
 * cat: tag, 比如 " 华语 "、" 古风 " 、" 欧美 "、" 流行 ", 默认为"全部",
 * order (new, hot)
 * cat ( " 华语 "、" 古风 " 、" 欧美 "、" 流行 ")
 * @returns
 */
export const _topPlaylist = (params?: object) => {
  return http.request<dataType>('get', '/top/playlist', { params })
}

/**
 *  精品歌单标签
 * @param params
 * limit
 * cat ( " 华语 "、" 古风 " 、" 欧美 "、" 流行 ")
 * @returns
 */
export const _highqualityTags = (params?: object) => {
  return http.request<dataType>('get', '/playlist/highquality/tags', { params })
}

/**
 *  获取精品歌单
 * @param params
 * limit
 * cat ( " 华语 "、" 古风 " 、" 欧美 "、" 流行 ")
 * @returns
 */
export const _highqualityPlaylist = (params?: object) => {
  return http.request<dataType>('get', '/top/playlist/highquality', { params })
}

/**
 *  网友精品歌单
 * @param params
 * id 歌单 id
 * @returns
 */
export const _relatedPlaylist = (params?: object) => {
  return http.request<dataType>('get', '/related/playlist', { params })
}

/**
 *  推荐歌单（未登录）
 * @param params limit
 * @returns
 */
export const _personalized = (params?: object) => {
  return http.request<dataType>('get', '/personalized', { params })
}

/**
 *  推荐新音乐（未登录）
 * @param params
 * limit:  取出数量 , 默认为 10
 * @returns
 */
export const _personalizedNewsong = (params?: object) => {
  return http.request<dataType>('get', '/personalized/newsong', { params })
}

/**
 *  推荐电台（未登录）
 * @param params
 * @returns
 */
export const _personalizedDjprogram = (params?: object) => {
  return http.request<dataType>('get', '/personalized/djprogram', { params })
}

/**
 *  推荐节目（未登录）
 * @param params
 * @returns
 */
export const _programRecommend = (params?: object) => {
  return http.request<dataType>('get', '/program/recommend', { params })
}

/**
 *  独家放送（入口列表）
 * 说明 : 调用此接口 , 可获取独家放送
 * @param params
 * @returns
 */
export const _personalizedPrivatecontent = (params?: object) => {
  return http.request<dataType>('get', '/personalized/privatecontent', {
    params
  })
}

/**
 *  独家放送
 * 说明 : 调用此接口 , 可获取独家放送列表
 * @param params
 * limit: 返回数量 , 默认为 60
 * offset: 偏移数量，用于分页 , 如 :( 页数 -1)\*60, 其中 60 为 limit 的值 , 默认为 0
 * @returns
 */
export const _personalizedPrivatecontentList = (params?: object) => {
  return http.request<dataType>('get', '/personalized/privatecontent/list', {
    params
  })
}

/**
 *  获取推荐视频
 * 调用此接口, 可获取推荐视频,分页参数只能传入 offset
 * @param params
 * offset: 默认 0
 * @returns
 */
export const _videoRecommend = (params?: object) => {
  return http.request<dataType>('get', '/video/timeline/recommend', {
    params
  })
}

/**
 *  电台个性推荐
 * 调用此接口,可获取电台个性推荐列表
 * @param params
 * limit: 返回数量,默认为 6,总条数最多 6 条
 * @returns
 */
export const _djRecommend = (params?: object) => {
  return http.request<dataType>('get', '/dj/personalize/recommend', {
    params
  })
}

/**
 *  获取所有榜单
 * @param params
 * limit: 返回数量,默认为 6,总条数最多 6 条
 * @returns
 */
export const _toplist = (params?: object) => {
  return http.request<dataType>('get', '/toplist', {
    params
  })
}

/**
 *  获取所有榜单
 * @param params
 * limit: 返回数量,默认为 6,总条数最多 6 条
 * @returns
 */
export const _toplistDetail = (params?: object) => {
  return http.request<dataType>('get', '/toplist/detail', {
    params
  })
}

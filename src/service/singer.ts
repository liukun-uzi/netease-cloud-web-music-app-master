import http from './http/request'

type dataType = {
  data?: any
  code: number
  artists: any[]
}

/**
 *  歌手分类列表
 * @param params
 * limit: 返回数量 , 默认为 30
 * offset: 偏移数量，用于分页 , 如 :( 页数 -1)\*30, 其中 30 为 limit 的值 , 默认为 0
 * initial：按首字母索引查找参数
 * type: -1:全部 1:男歌手 2:女歌手 3:乐队
 * area: -1:全部 7华语 96欧美 8:日本 16韩国 0:其他
 * @returns
 */
export const _artistList = (params?: object) => {
  return http.request<dataType>('get', '/artist/list', {
    params
  })
}

import http from './http/request'

type dataType = {
  data?: any
  code: number
  banners?: any[]
  result?: any
  songs: any
  playlist: any
  ids: number[]
}

type lyricType = {
  lrc: any
  lyricUser: any
  tlyric: any
  transUser: any
}

/**
 *  获取歌单详情
 * 返回的trackIds是完整的，tracks 则是不完整的，可拿全部 trackIds 请求一次 song/detail 接口获取所有歌曲的详情
 * @param params
 * id : 歌单 id
 * s : 歌单最近的 s 个收藏者
 * @returns
 */
export const _playlistDetail = (params?: object) => {
  return http.request<dataType>('get', '/playlist/detail', { params })
}

/**
 *  获取歌单所有歌曲
 * @param params
 * id : 歌单 id
 * limit: 限制获取歌曲的数量，默认值为当前歌单的歌曲数量
 * offset:
 * @returns
 */
export const _listByPlaylist = (params?: object) => {
  return http.request<dataType>('get', '/playlist/track/all', { params })
}

/**
 *  获取音乐详情
 * @param params
 * ids: 支持多个 id
 * @returns
 */
export const _songDetail = (params?: object) => {
  return http.request<dataType>('get', '/song/detail', { params })
}

/**
 *  获取音乐url
 * @param params
 * id : 音乐 id(array)
 * br ：码率,默认设置了 999000 即最大码率,如果要 320k 则可设置为 320000,其他类推
 * @returns
 */
export const _songUrl = (params?: object) => {
  return http.request<dataType>('get', '/song/url', { params })
}

/**
 *  检查音乐是否可用
 * @param params
 * id : 音乐 id(array)
 * br ：码率,默认设置了 999000 即最大码率,如果要 320k 则可设置为 320000,其他类推
 * @returns
 */
export const _checkMusic = (params?: object) => {
  return http.request<dataType>('get', '/check/music', { params })
}

/**
 *  获取音乐歌词
 * @param params
 * id : 音乐 id
 * @returns
 */
export const _lyric = (params?: object) => {
  return http.request<lyricType>('get', '/lyric', { params })
}

/**
 *  喜欢音乐
 *
 * @param params
 * id : 歌曲 id
 * like: true or false
 * @returns
 */
export const _like = (params?: object) => {
  return http.request<dataType>('get', '/like', { params })
}

/**
 *  喜欢音乐列表
 * 已喜欢音乐 id 列表
 * @param params
 * uid : 用户 id
 * @returns
 */
export const _likelist = (params?: object) => {
  return http.request<dataType>('get', '/likelist', {
    params
  })
}

/**
 *  获取用户歌单
 * 已喜欢音乐 id 列表
 * @param params
 * uid : 用户 id
 * limit: 返回数量 , 默认为 30
 * offset: 偏移数量，用于分页 , 如 :( 页数 -1)\*30, 其中 30 为 limit 的值 , 默认为 0
 * @returns
 */
export const _userPlaylist = (params?: object) => {
  return http.request<dataType>('get', '/user/playlist', {
    params
  })
}

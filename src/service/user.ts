import http from './http/request'

type dataType = {
  data?: any
  code: number
  cookie?: string
}

export const _login = (params?: object) => {
  return http.request<dataType>('get', '/login/cellphone', { params })
}
// 发送验证
export const _sentCaptcha = (params?: object) => {
  return http.request<dataType>('get', '/captcha/sent', { params })
}
// 验证验证码
export const _captchaVerify = (params?: object) => {
  return http.request<dataType>('get', '/captcha/verify', { params })
}
// 获取登录状态
export const _loginStatus = (data = {}) => {
  return http.request<dataType>('post', '/login/status', { data })
}
// 生成二维码key
export const _createKey = (params = {}) => {
  return http.request<dataType>('post', '/login/qr/key', { params })
}
// 生成二维码
export const _createQr = (params?: object) => {
  return http.request<dataType>('post', '/login/qr/create', { params })
}
// 二维码检测扫码状态接口
export const _checkQr = (params?: object) => {
  return http.request<dataType>('get', '/login/qr/check', { params })
}

import { defineStore } from 'pinia'
import { store } from '@/store'
import { useCommonStoreHook } from '@/store/modules/common'
import { recommendType } from './types'
import {
  _banner,
  _personalized,
  _recommendSongs,
  _recommendResource,
  _topPlaylist,
  _personalizedNewsong,
  _highqualityPlaylist,
  _hotCatlist,
  _catlist,
  _toplistDetail
} from '@/service/recommend'

import { _listByPlaylist } from '@/service/song'
const commonStore = useCommonStoreHook()

export const useRecommendStore = defineStore({
  id: 'music-recommend',
  state: (): recommendType => ({
    banners: [],
    recommendSongs: [],
    recommendList: [],
    hotPlaylist: [],
    newPlaylist: [],
    newSongs: [],
    // 第二页歌单数据
    highquality: null,
    hotCatlist: [],
    cats: {
      all: {},
      sub: [],
      categories: []
    },
    // 第三页排行榜数据
    glist: [],
    qlist: [],
    worldlist: []
  }),
  actions: {
    // 获取发现音乐页全部数据
    async initRecommendData() {
      this.initBanner()
      this.initRecommendList()
      this.initPlayList('new')
      this.initPlayList('hot')
      this.initNewMusic(12)
    },
    initBanner() {
      _banner().then((res) => {
        this.banners = res.banners
      })
    },
    async initRecommendList() {
      const userId = commonStore.profile.userId
      if (!userId) {
        _personalized({ limit: 10 }).then((res) => {
          this.recommendList = res.result
        })
      } else {
        await _recommendResource({ limit: 9 }).then((res) => {
          this.recommendList = res.recommend
        })
        _recommendSongs().then((res) => {
          if (res.code === 200) {
            const song = res.data.dailySongs[0]
            const recommend = {
              picUrl: song.al.picUrl,
              name: '每日歌曲推荐',
              id: 1
            }
            this.recommendList.unshift(recommend)
            this.recommendSongs = res.data.dailySongs
          }
        })
      }
    },
    initPlayList(order: 'new' | 'hot', limit = 10, offset = 0, cat = '') {
      _topPlaylist({ order, limit, offset, cat }).then((res) => {
        this[`${order}Playlist`] = res.playlists
      })
    },
    initNewMusic(limit: number) {
      _personalizedNewsong({ limit }).then((res) => {
        this.newSongs = res.result
      })
    },
    // 获取第二页歌单全部数据
    async initPlaylistData() {
      this.initHighqualityPlaylist()
      this.initHotCatlist()
    },
    initHighqualityPlaylist() {
      _highqualityPlaylist({ limit: 1 }).then((res) => {
        this.highquality = res.playlists[0]
      })
    },
    // 热门歌单分类 接口调用优先级 p0
    initHotCatlist() {
      _hotCatlist().then((res) => {
        this.hotCatlist = res.tags
        this.initCatlist()
      })
    },
    // 接口调用优先级 p1
    // 歌单分类
    initCatlist() {
      _catlist().then((res) => {
        this.cats = {
          all: res.all,
          sub: res.sub,
          categories: res.categories
        }
      })
    },
    async initRanklistData() {
      this.initRankDetail()
    },
    async initRankDetail() {
      this.glist = []
      const ids = [
        1978921795, 71385702, 991319590, 5059633707, 5059661515, 10520166,
        71384707, 5059642708
      ]
      const res = await _toplistDetail()
      res.list.forEach(async (ele) => {
        if (ele.ToplistType) {
          const newItem = await this.insertTracks(ele)
          this.glist.push(newItem)
        } else if (ids.includes(ele.id)) {
          this.qlist.push(ele)
        } else {
          this.worldlist.push(ele)
        }
      })
    },
    // 对ranks官方榜添加
    insertTracks(item: any) {
      return new Promise((resolve) => {
        _listByPlaylist({ id: item.id, limit: 5 }).then((res) => {
          item.tracks = res.songs
          resolve(item)
        })
      })
    }
  }
})

export const useRecommendHook = () => {
  return useRecommendStore(store)
}

interface profile {
  userId: number
  nickname: string
  avatarImgId: number
  avatarUrl: string
  backgroundUrl: string
}

export type commonType = {
  theme: string
  account: object
  profile: profile
  cookie: string
}

export type recommendType = {
  banners: any[]
  recommendSongs: any[]
  recommendList: any[]
  hotPlaylist: any[]
  newPlaylist: any[]
  newSongs: any[]
  // 第二页歌单
  highquality: any
  hotCatlist: any[]
  cats: catsType
  // 第三页排行榜
  glist: playlistType[]
  qlist: playlistType[]
  worldlist: playlistType[]
}

interface catsType {
  sub: any[]
  categories: string[]
  all: any
}

export type userType = {
  userPlaylist: any[]
  like: any
  likeIds: number[]
  subscribeList: any[]
}

// 音乐播放器关联数据
export type songType = {
  songList: any[]
  currentSongDetail: songItem
  currentSongId: number
  currentLyric: lyricType
  currentTime: number
  lyricOpen: boolean
  status: boolean
  volume: number
}

export interface lyricType {
  lrc: any
  lyricUser: any
  tlyric: any
  transUser: any
}

export interface songItem {
  id?: number
  url?: string
  al: any
  fee?: number
  ar?: any
  alia?: string[]
  tns: string[]
  name: string
}

export type playlistType = {
  id: number
  playCount: number
  coverImgUrl: string
  name: string
  creator?: any
  picUrl?: string
  playcount?: number
  tracks?: any[]
  updateTime?: number
}

export const formatNumber = (value: number) => {
  const k = 10000
  const sizes = ['', '万', '亿']
  let i: any
  if (value < k) {
    return value + ''
  } else {
    i = Math.floor(Math.log(value) / Math.log(k))
    return (value / Math.pow(k, i)).toFixed(0) + sizes[i]
  }
}

export const formatSecond = (second: number) => {
  let hour_str = `${Math.floor(second / 60)}`
  let second_str = `${Math.ceil(second % 60)}`
  if (hour_str.length === 1) {
    hour_str = `0${hour_str}`
  }
  if (second_str.length === 1) {
    second_str = `0${second_str}`
  }
  return `${hour_str}:${second_str}`
}

export const parseLyric = (text: string) => {
  const result = []
  let lines = text.split('\n')
  const pattern = /\[\d{2}:\d{2}.\d+\]/g
  if (!pattern.test(lines[0])) {
    lines = lines.slice(1)
  }
  lines[lines.length - 1].length === 0 && lines.pop()
  lines.forEach((ele) => {
    const time = ele.match(pattern)
    const value = ele.replace(pattern, '')
    time.forEach((e) => {
      const t = e.slice(1, -1).split(':')
      result.push([parseInt(t[0], 10) * 60 + parseFloat(t[1]), value])
    })
  })
  result.sort(function (a, b) {
    return a[0] - b[0]
  })
  return result
}

export const sliceNull = (lrc: string[]) => {
  const result = []
  for (const item of lrc) {
    if (item[1]) {
      result.push(item)
    }
  }
  return result
}

export const generateUpperCaseArray = (): string[] => {
  const lettersArray: string[] = []
  for (let i = 65; i <= 90; i++) {
    lettersArray.push(String.fromCharCode(i))
  }
  return lettersArray
}
